<?php

use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProjectsController;
use App\Http\Controllers\TasksController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[PagesController::class, 'index']);

/* Tasks */
Route::get('/tasks', [TasksController::class, 'index'])->name('tasks.index');
Route::get('/tasks/create', [TasksController::class, 'create'])->name('tasks.create');
Route::post('/tasks', [TasksController::class, 'store'])->name('tasks.store');
Route::get('/tasks/show/{id}', [TasksController::class, 'show'])->name('tasks.show');
Route::get('/tasks/edit/{id}', [TasksController::class, 'edit'])->name('tasks.edit');
Route::post('/tasks/update/{id}', [TasksController::class, 'update'])->name('tasks.update');
Route::get('/tasks/delete/{id}', [TasksController::class, 'destroy'])->name('tasks.delete');
Route::post('/tasks/togglecomplete/{id}', [TasksController::class, 'taskCompletedToggle'])->name('taskscompletetoggle');
Route::post('/update/ordernumbers', [TasksController::class, 'updatedOrderNumbers'])->name('tasks.updatedOrderNumbers');

/* Projects */
Route::get('/projects', [ProjectsController::class, 'index'])->name('projects.index');
Route::get('/projects/create', [ProjectsController::class, 'create'])->name('projects.create');
Route::post('/projects', [ProjectsController::class, 'store'])->name('projects.store');
Route::get('/projects/show/{id}', [ProjectsController::class, 'show'])->name('projects.show');
Route::get('/projects/edit/{id}', [ProjectsController::class, 'edit'])->name('projects.edit');
Route::post('/projects/update/{id}', [ProjectsController::class, 'update'])->name('projects.update');
Route::get('/projects/delete/{id}', [ProjectsController::class, 'destroy'])->name('projects.delete');
Route::get('/projects/{id}', [ProjectsController::class, 'viewprojecttasks'])->name('projects.viewtasks');

