## The Assignment


Create task (info to save: task name, priority, timestamps)
Edit task
Delete task
Reorder tasks with drag and drop in the browser. Priority should automatically be updated based on this. #1 priority goes at top, #2 next down and so on.
Tasks should be saved to a mysql table.
Add project functionality to the tasks. User should be able to select a project from a dropdown and only view tasks associated with that project.

You will be graded on how well-written & readable your code is, if it works, and if you did it the Laravel way.

Include any instructions on how to set up & deploy the web application in your Readme.md file in the project directory (delete the default readme).

## Installation

- Make sure you have an active internet network. The styles and are called from a CDN
- Unzip the file inside your LAMPP or XAMPP stacks to run locally
- Run 'composer update'
- Run 'php artisan migrate' to create the table structure.
- Run 'php artisan db:seed' to run database seeds for the priorities and taskstatuses tables 
(There are no CRUD functions for these models. So only the database seeds can enter data into these tables. Only run the seeds once.)
- Run 'php artisan serve' to start the laravel server and view project in the browser with pre-filled data in the users, priorities and taskstatuses tables
- Clear app cache from zipping. 'php artisan route:cache', 'php artisan cache:clear'

## Models 

The application has the following models and fluent methods in their respective controllers
- priority.php (No CRUD Function. Use DB:Seeds)
- projects.php (Create, Read, Update, Delete. On delete, all projects_id in associated tasks are set to null).
- tasks.php (Create, Read, Update, Delete. Order of priority can be altered by dragging and dropping on the task displayed per project. Tasks can also be marked as completed)
- taskstatus.php (No CRUD Function. Use DB:Seeds)
- User.php (No CRUD Function. Use DB:Seeds)

## Third party tools

Third party assets were used in the development of the front end and are referenced below:

Bootstrap Starter Kit
Jquery
Jqueryui:sortable


## Resources consulted for learning

Stack Overflow
JqueryUi Tutorials
