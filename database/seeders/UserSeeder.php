<?php

namespace Database\Seeders;

use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* DB::table('users')->insert([
            'name' => 'Uchenna Joel'.Str::random(3),
            'email' => 'uchenna.joel'.Str::random(3).'@coalitiontest.com',
            'password' => Hash::make('password'),
        ]); */

        User::factory()->count(10)->create();
    }
}
