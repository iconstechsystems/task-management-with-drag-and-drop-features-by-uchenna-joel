<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taskstatuses')->insert([
            'title' => 'On-Going'
        ]);
        DB::table('taskstatuses')->insert([
            'title' => 'Completed'
        ]);
        DB::table('taskstatuses')->insert([
            'title' => 'Overdue'
        ]);
    }
}
