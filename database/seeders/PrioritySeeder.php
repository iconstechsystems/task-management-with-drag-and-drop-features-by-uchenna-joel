<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PrioritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('priorities')->insert([
            'title' => 'Not Urgent'
        ]);
        DB::table('priorities')->insert([
            'title' => 'Fairly Urgent'
        ]);
        DB::table('priorities')->insert([
            'title' => 'Urgent'
        ]);
        DB::table('priorities')->insert([
            'title' => 'Very Urgent'
        ]);
    }
}
