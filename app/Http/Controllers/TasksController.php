<?php

namespace App\Http\Controllers;

use App\Models\priority;
use App\Models\projects;
use App\Models\tasks;
use App\Models\taskstatus;
use App\Models\User;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // list all tasks
        $tasks = tasks::orderBy('ordernum', 'asc')->get();
        return view('tasks.index')->with(['tasks' => $tasks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // show form to create task
        $priorities = priority::get();
        $users = User::get();
        $projects = projects::get();
        return view('tasks.create')->with(['priorities' => $priorities, 'users' => $users, 'projects' => $projects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // store task
        $request->validate([
            'title' => 'required|string',
            'deadline' => 'required|date|after:today',
            'priority' => 'required',
            'assignee' => 'required',
        ]);

        // dd($request);

        $addTask = new tasks;
        $addTask->title = $request->title;
        $addTask->deadline = $request->deadline;
        $addTask->priority = $request->priority;
        $addTask->status = 1;
        $addTask->assignee = $request->assignee;
        $addTask->projects_id = $request->projects_id;
        $addTask->save();

        return back()->with(['Task added successfully'])->with(['success' => 'Task created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = tasks::findOrFail($id);
        $taskstatus = taskstatus::findOrFail($task->status);
        $priority = priority::findOrFail($task->priority);
        $assignee = User::findOrFail($task->assignee);
        if (isset($task->projects_id)) {
            $project = projects::findOrFail($task->projects_id);
        }else{
            $project = null;
        }
        return view('tasks.show')->with(['taskstatus' => $taskstatus, 'assignee' => $assignee, 'priority' => $priority, 'task' => $task, 'project' => $project]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = tasks::findOrFail($id);
        $taskstatuses = taskstatus::get();
        $priorities = priority::get();
        $users = User::get();
        $projects = projects::get();
        return view('tasks.edit')->with(['taskstatuses' => $taskstatuses, 'users' => $users, 'priorities' => $priorities, 'task' => $task, 'projects' => $projects]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // store task
        $request->validate([
            'title' => 'required|string',
            'status' => 'required',
            'priority' => 'required',
            'assignee' => 'required',
        ]);
        $addTask = tasks::findOrFail($id);
        if (isset($request->deadline)) {
            $request->validate([
                'deadline' => 'required|date|after:yesterday'
            ]);
            $addTask->deadline = $request->deadline;
        }

        $addTask->title = $request->title;
        $addTask->priority = $request->priority;
        $addTask->status = $request->status;
        $addTask->assignee = $request->assignee;
        $addTask->projects_id = $request->projects_id;

        # set completion date to null if task overdue

        $addTask->save();

        return back()->with(['success' => 'Task "'.$addTask->title.'" has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = tasks::findOrFail($id);
        $task->delete();
        return redirect()->route('tasks.index')->with(['success' => 'Task deleted successfully']);
    }

    public function taskCompletedToggle(Request $request, $id){
        $task = tasks::findOrFail($id);
        // echo $task->title;
        if (isset($request->id)) {
            if ($task->status == 2) {
                # task already completed mark as on-going
                $task->status = 1;
                $firstid = tasks::first();
                $task->ordernum = 0;
                $responsemessage = 'Task "'.$task->title.'" marked as On-Going';
                $task->completiondate = null;
                $currentstatus = null;
            }else{
                # task is completed mark as completed
                $task->status = 2;
                $responsemessage = 'Task "'.$task->title.'" marked as completed';
                # when marked as complete set the order number to the last id on the recod
                # this was the completed items are always on the end of the page
                $lastid = tasks::latest()->first();
                $lastid = $lastid->id;
                $task->ordernum = $lastid;
                $task->completiondate = now();
                $currentstatus = "Completed";
            }
        }
        $task->save();
        return response()->json(['success' => $responsemessage, 'currentstatus' => $currentstatus]);
    }

    public function updatedOrderNumbers(Request $request){
        if (isset($request->update)) {
            # loop through the order positions available
            foreach ($request->ordernumbers as $currentposition) {
                # update the table with the id and ordernums, recall that the
                # currentindex and the data ordernumbers were sent through ajax
                $id = $currentposition[0];
                $ordernum = $currentposition[1];

                $task = tasks::find($id);
                $task->ordernum = $ordernum;
                $task->save();
            }

            exit('success');
        }
    }
}
