<?php

namespace App\Http\Controllers;

use App\Models\projects;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        $title = "Coalition Test Assignment";
        $projects = projects::get();
        return view('welcome')->with(['title' => $title, 'projects' => $projects]);
    }
}
