<?php

namespace App\Http\Controllers;

use App\Models\projects;
use App\Models\tasks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = projects::get();
        return view('projects.index')->with(['projects' => $projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'string|required',
            'description' => 'string|required',
            'startdate' => 'required|date|after:today',
            'enddate' => 'required|date|after:startdate'
        ]);

        $project = new projects;
        $project->title = $request->title;
        $project->description = $request->description;
        $project->startdate = $request->startdate;
        $project->enddate = $request->enddate;
        $project->save();

        return back()->with(['success' => 'Project added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = projects::find($id);
        return view('projects.show')->with(['project' => $project]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = projects::find($id);
        return view('projects.edit')->with(['project' => $project]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'string|required',
            'description' => 'string|required',
        ]);

        $project = projects::find($id);
        $project->title = $request->title;
        $project->description = $request->description;
        if (isset($request->startdate)) {
            $request->validate(['startdate' => 'date|after:today']);
            $project->startdate = $request->startdate;
        }

        if (isset($request->enddate)) {
            $request->validate(['enddate' => 'date|after:startdate']);
            $project->enddate = $request->enddate;
        }

        $project->save();

        return back()->with(['success' => 'Project updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = projects::find($id);
        if (tasks::whereprojectsId($id)->exists()) {
            # set all tasks with ths project to null
            //dd('p exists');
            $tasks = tasks::whereprojectsId($id)->get();
            foreach($tasks as $taskitem){
                $task = tasks::find($taskitem->id);
                $task->projects_id = null;
                $task->save();
            }
            // DB::table('tasks')->selectRaw('UPDATE tasks SET projects_id = null WHERE projects_id = ?', [$id]);
        }
        $project->delete();
        return back()->with(['success' => 'Project deleted successfully and associated tasks have been updated accordingly']);
    }

    /* View Project Tasks */
    public function viewprojecttasks($id){
        $project = projects::findOrFail($id);
        $tasks = tasks::where('projects_id', '=',$project->id)->orderBy('ordernum', 'asc')->get();
        $projects = projects::get();
        return view('welcome')->with(['project' => $project, 'tasks' => $tasks, 'projects' => $projects]);
    }
}
