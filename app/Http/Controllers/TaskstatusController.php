<?php

namespace App\Http\Controllers;

use App\Models\taskstatus;
use Illuminate\Http\Request;

class TaskstatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\taskstatus  $taskstatus
     * @return \Illuminate\Http\Response
     */
    public function show(taskstatus $taskstatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\taskstatus  $taskstatus
     * @return \Illuminate\Http\Response
     */
    public function edit(taskstatus $taskstatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\taskstatus  $taskstatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, taskstatus $taskstatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\taskstatus  $taskstatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(taskstatus $taskstatus)
    {
        //
    }
}
