@extends('layouts.main')

@section('content')
<div class="mt-5">
    <h3>All Projects <a href="{{ route('projects.create') }}" class="btn btn-outline-primary">Create Project</a></h3>
    @if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('success') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <div class="table-responsive">
        <table class="table table-dark">
            <thead>
                <th># Project ID</th>
                <th>Title</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($projects as $item)
                    {{-- Loop through the task records and display data --}}
                    <tr>
                        <td >{{ $item->id }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->startdate }}</td>
                        <td>{{ $item->enddate }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('projects.show', ['id' => $item->id]) }}" class="btn btn-primary active"
                                    aria-current="page">View
                                    </a>
                                <a data-bs-toggle="modal" data-bs-target="#deleteProject{{ $item->id }}" type="button"
                                    class="btn btn-danger">Delete </a>
                                <!-- Delete Task Modal -->
                                @include('projects.delete')
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <th># Project ID</th>
                <th>Title</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Action</th>
            </tfoot>
        </table>

    </div>
</div>
@endsection
