@extends('layouts.main')

@section('content')
<div class="mt-5">
    <h3>Project Information <a href="{{ route('projects.edit', ['id' => $project->id]) }}" class="btn btn-outline-primary">Edit this Project</a></h3>
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    @if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('success') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif
    <table class="table table-hover">
        <thead>
            <th># Project ID</th>
            <th>Title</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Description</th>
            <th>Created on</th>
            <th>Last Updated on</th>
        </thead>
        <tbody>
            <tr>
                <td>{{ $project->id }}</td>
                <td>{{ $project->title }}</td>
                <td>{{ $project->startdate }}</td>
                <td>{{ $project->enddate }}</td>
                <td>{{ $project->description }}</td>
                <td>{{ $project->created_at }}</td>
                <td>{{ $project->updated_at }}</td>
            </tr>
        </tbody>
        <tfoot>
            <th># Project ID</th>
            <th>Title</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Description</th>
            <th>Created on</th>
            <th>Last Updated on</th>
        </tfoot>
    </table>
</div>
@endsection
