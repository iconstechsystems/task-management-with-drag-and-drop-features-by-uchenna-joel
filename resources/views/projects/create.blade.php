@extends('layouts.main')

@section('content')
<div class="mt-5">
    <h3>Create a Project</h3>
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    @if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('success') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif
    <form action="{{ route('projects.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="startdate">Start Date</label>
                    <input type="date" name="startdate" id="startdate" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="enddate">End Date</label>
                    <input type="date" name="enddate" id="enddate" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="Description">Description</label>
                    <textarea name="description" id="description" class="form-control" rows="3"></textarea>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <button type="submit" class="btn btn-primary">Create Record</button>
            </div>
        </div>
    </form>
</div>
@endsection
