<div class="modal fade" id="deleteProject{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: black;">Delete Project</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body" style="color: black;">
                This action is irreversible. <br> Are you sure you want to delete this record?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                    data-bs-dismiss="modal">No, Close</button>
                <a href="{{ route('projects.delete', ['id' => $item->id]) }}" class="btn btn-danger">Yes, Delete</a>
            </div>
        </div>
    </div>
</div>
