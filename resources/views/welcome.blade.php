@extends('layouts.main')

@section('content')
<div class="mt-5 text-center">

    <h3>Select a project from the dropdown below to view tasks </h3>

    <div class="dropdown">

        <button class="btn btn-secondary btn-block dropdown-toggle" type="button" id="dropdownMenuButton1"
            data-bs-toggle="dropdown" aria-expanded="false">
            Select Project
        </button>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            @foreach ($projects as $item)
            <li><a class="dropdown-item" href="{{ route('projects.viewtasks', ['id' => $item->id]) }}">{{ $item->title }}</a></li>
            @endforeach
        </ul>

    </div>

    @if (isset($project))

        <div class="mt-3 mb-3">
            Project Title: {{ $project->title }} <br>
            Project Start Date: {{ \Carbon\Carbon::parse($project->startdate)->toDayDateTimeString() }} <br>
            Project End Date: {{ \Carbon\Carbon::parse($project->enddate)->toDayDateTimeString() }} <br>
            Project Description: {{ $project->description }} <br>
        </div>

    @endif

    @if (isset($tasks))

        @if (count($tasks))
        
            @include('components.sortabletasks')

        @else

            <h4>There are currently no tasks assigned to this project</h4>

        @endif
        
    @endif
</div>
@endsection
