<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- CSS Files -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
    <link rel="stylesheet" href="assets/css/style.css">

    {{-- JS Files --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script>
        $(document).ready(function(){
            // sort the rows and update the order numbers
            $('table tbody').sortable({
                update: function( event, ui ){
                    $(this).children().each(function (index) {
                        if ($(this).attr('data-ordernumber') != (index+1)) {
                            $(this).attr('data-ordernumber', (index+1)).addClass("taskupdated");
                        }
                    });
                    updateOrderNumber(); // OOP mechanism, call the function to update the order numbers
                }
            });

            // update order numbers of task rows using data attributes
            function updateOrderNumber(){
                var ordernumbers = [];
                var _token = $('meta[name="csrf-token"]').attr('content');
                $('.taskupdated').each(function () {
                    ordernumbers.push([$(this).attr('data-index'), $(this).attr('data-ordernumber')]);
                    $(this).removeClass('updated');
                });

                $.ajax({
                    url: '/update/ordernumbers',
                    method: 'POST',
                    dataType: 'text',
                    data: {
                        update: 1,
                        ordernumbers: ordernumbers,
                        _token:_token
                    }, success: function(response){
                        console.log(response);
                    }
                });
            }
        });

        function markAsCompleted(id){
            // use an ajax call to mark this data as completed and add completed class to the row
            var _token = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: '/tasks/togglecomplete/'+id,
                method: 'POST',
                dataType: 'text',
                data: {
                    id:id,
                    _token:_token
                }, success: function(response){
                    console.log(response);
                    if (response) {
                        location.reload(true);
                    }
                }
            });
        }
    </script>

    {{-- Page Title --}}
    <title>Coalition Technologies Laravel Test for Uchenna Joel</title>
</head>

<body>
    {{-- Navigation --}}
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ url('/') }}">Home</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll"
                aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarScroll">
                <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('tasks.index') }}">Tasks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('projects.index') }}">Projects</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto ">
                {{-- Blad yield --}}
                @yield('content')
            </div>
        </div>
    </div>
    <footer class="footer mt-5 py-3 bg-light">
        <div class="container">
          <span class="text-muted">This project is a test by coalition Technologies for Uchenna Joel</span>
        </div>
      </footer>

</body>

</html>
