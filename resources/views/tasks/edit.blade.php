@extends('layouts.main')

@section('content')
<div class="mt-5">
    <h3>Edit This Task</h3>
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    @if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('success') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif
    <form action="{{ route('tasks.update', ['id' => $task->id]) }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" value="{{ $task->title }}" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="deadline">Deadline: {{ $task->deadline }}</label>
                    <input type="date" name="deadline" id="deadline" class="form-control">
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="priority">Priority</label>
                    <select class="form-control" name="priority" required>
                        <option value="">Select Priority</option>
                        @foreach ($priorities as $item)
                        <option value="{{ $item->id }}" @if($task->priority == $item->id) selected @endif>{{ $item->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="status">Update Status</label>
                    <select class="form-control" name="status" required>
                        <option value="">Update Status</option>
                        @foreach ($taskstatuses as $item)
                        <option value="{{ $item->id }}" @if($task->status == $item->id) selected @endif>{{ $item->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="assignee">Assignee</label>
                    <select class="form-control" name="assignee" required>
                        <option value="">Assign task to a User</option>
                        @foreach ($users as $item)
                        <option value="{{ $item->id }}" @if($task->assignee == $item->id) selected @endif>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="project">Projects</label>
                    <select class="form-control" name="projects_id">
                        @if (count($projects))
                            <option value="">Assign task to a project</option>
                            @foreach ($projects as $item)
                                <option value="{{ $item->id }}" @if($task->projects_id == $item->id) selected @endif>{{ $item->title }}</option>
                            @endforeach
                        @else
                            <option value="">There are no projects at this time. Proceed to add tasks. You can update later</option>
                        @endif

                    </select>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <button type="submit" class="btn btn-warning">Update Record</button>
            </div>
        </div>
    </form>
</div>
@endsection
