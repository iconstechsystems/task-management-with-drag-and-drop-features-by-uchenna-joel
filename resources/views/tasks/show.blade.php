@extends('layouts.main')

@section('content')
<div class="mt-5">
    <h3>Show Tasks <a href="{{ route('tasks.edit', ['id' => $task->id]) }}" class="btn btn-outline-primary">Edit this Task</a></h3>
    <div class="card">
        <div class="card-header">
            Task Information
        </div>
        <div class="card-body">
            <table class="table table-hover table-striped">
                <thead>
                    <th># Task ID</th>
                    <th>Priority</th>
                    <th>Assignee</th>
                    <th>Deadline</th>
                    <th>Project</th>
                    <th>Status</th>
                    <th>Created On</th>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $task->id }}</td>
                        <td>{{ $priority->title }}</td>
                        <td>{{ $assignee->name }}</td>
                        <td>{{ \Carbon\Carbon::parse($task->deadline)->diffForHumans() }}</td>
                        <td>
                            @if (isset($project->title))
                                {{ $project->title }}
                            @else
                                Task has not been assigned to a project
                            @endif
                        </td>
                        <td>{{ $taskstatus->title }}</td>
                        <td>{{ \Carbon\Carbon::parse($task->created_at)->diffForHumans() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
