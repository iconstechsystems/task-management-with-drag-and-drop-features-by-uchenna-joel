@extends('layouts.main')

@section('content')
<div class="mt-5">
    <h3>Tasks <a href="{{ route('tasks.create') }}" class="btn btn-outline-primary">Create Task</a></h3>
    @include('components.sortabletasks')
</div>
@endsection
