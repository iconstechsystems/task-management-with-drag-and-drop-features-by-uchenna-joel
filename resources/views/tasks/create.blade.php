@extends('layouts.main')

@section('content')
<div class="mt-5">
    <h3>Create a Task</h3>

    {{-- Display validation errors when creating a task --}}
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    {{-- Display success messages after successful save --}}
    @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ session('success') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    {{-- Create task form --}}
    <form action="{{ route('tasks.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="deadline">Deadline</label>
                    <input type="date" name="deadline" id="deadline" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="priority">Priority</label>
                    <select class="form-control" name="priority" required>
                        <option value="">Select Priority</option>
                        @foreach ($priorities as $item)
                        <option value="{{ $item->id }}">{{ $item->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="assignee">Assignee</label>
                    <select class="form-control" name="assignee" required>
                        <option value="">Assign task to a User</option>
                        @foreach ($users as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="form-group">
                    <label for="project">Projects</label>
                    <select class="form-control" name="projects_id">
                        @if (count($projects))
                            <option value="">Assign task to a project</option>
                            @foreach ($projects as $item)
                                <option value="{{ $item->id }}">{{ $item->title }}</option>
                            @endforeach
                        @else
                            <option value="">There are no projects at this time. Proceed to add tasks. You can update later</option>
                        @endif

                    </select>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <button type="submit" class="btn btn-primary">Create Record</button>
            </div>
        </div>
    </form>
</div>
@endsection
