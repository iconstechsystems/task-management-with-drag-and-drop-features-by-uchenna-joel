<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">

    <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
      <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
    </symbol>

</svg>
<div class="alert alert-primary d-flex align-items-center" role="alert">
    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
    <div>
        Tasks are ordered by task status. On-going tasks show first, However you can choose to place priority on a task
    by dragging up or down the table. Marking a project as completed moves it to the bottom of the priority list
    </div>
</div>
@if (session('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('success') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
<div class="table-responsive">
    <table class="table table-dark">
        <thead>
            <th># Task ID</th>
            <th>Title</th>
            <th>Completed?</th>
            <th>Status</th>
            <th>Deadline</th>
            <th>Project</th>
            <th>Priority</th>
            <th>Assignee</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach ($tasks as $item)
            {{-- Get linked data. I opted against inner joins because there are columns with similar names in the tables --}}
            <?php
                $status = \App\Models\taskstatus::find($item->status);
                $priority = \App\Models\priority::find($item->priority);
                $assignee = \App\Models\User::find($item->assignee);
                if (isset($item->projects_id)) {
                    $projects = \App\Models\projects::find($item->projects_id);
                }else{
                    $projects = null;
                }
            ?>

            {{-- Loop through the task records and display data --}}
            <tr data-index="{{ $item->id }}" data-ordernumber="{{ $item->ordernum }}">
                <td>{{ $item->id }}</td>
                <td>{{ $item->title }}</td>
                <td><input class="form-check-input me-1" data-taskid="{{ $item->id }}"
                        onclick="markAsCompleted({{ $item->id }})" type="checkbox" name="taskstatus{{ $item->id }}"
                        @if($item->status == '2') checked @endif value="2" id="taskstatus{{$item->id}}" ></td>
                <td><span
                        class="badge @if($item->status == '1') bg-primary @elseif($item->status == '2') bg-success @else bg-danger @endif rounded-pill">{{ $status->title }}
                        @isset($item->completiondate)
                        {{ \Carbon\Carbon::parse($item->completiondate)->diffForHumans() }} @endisset</span></td>
                <td>{{ \Carbon\Carbon::parse($item->deadline)->toDayDateTimeString() }} @if (\Carbon\Carbon::parse($item->deadline)->isPast()) <span class="badge bg-danger">Deadline is passed</span> @endif</td>
                <td>
                    @if (isset($projects))
                    {{ $projects->title }}
                    @endif
                </td>
                <td>{{ $priority->title }}</td>
                <td>{{ $assignee->name }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('tasks.show', ['id' => $item->id]) }}" class="btn btn-primary active"
                            aria-current="page">View
                        </a>
                        <a data-bs-toggle="modal" data-bs-target="#deleteTask{{ $item->id }}" type="button"
                            class="btn btn-danger">Delete </a>
                        <!-- Delete Task Modal -->
                        @include('tasks.delete')
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <th># Task ID</th>
            <th>Title</th>
            <th>Completed?</th>
            <th>Status</th>
            <th>Deadline</th>
            <th>Project</th>
            <th>Priority</th>
            <th>Assignee</th>
            <th>Action</th>
        </tfoot>
    </table>

</div>
